[![Build Status][badge-travis-image]][badge-travis-url]
## DTRACING (DEINN-TRACING)
El custom plugin dtracing esta basado en zipkin plugin. Se destingue por agregar una configuraci�n
para buscar el header especificado en la configuraci�n como ***tracing_header_name*** y generar el Uber-Trace-id
a partir del tracing_header_name.

### IMPORTANTE
- Si el header Uber-Trace-id se encuentra, se ignora la configuraci�n tracing_header_name.
- Este plugin solo aplica esta l�gica al seguimiento con Jaeger
- La documentaci�n del estandar para la generaci�n del Uber-Trace-Id se encuentra en https://www.jaegertracing.io/docs/1.27/client-libraries/
- Este custom plugin esta basado en el el plugin de Zipkin que probee Kong Gateway https://docs.konghq.com/hub/kong-inc/zipkin/

### Ejemplo de configuraci�n:
#### Kong API Admin
```
curl --request POST \
  --url http://127.0.0.1:8001/services/go.ms-go-web-image-orchestrator-kong.pnum-3000/plugins \
  --header 'Content-Type: application/json' \
  --data '{
  "enabled": true,
  "name": "dtracing",
  "config": {
    "service": "go.ms-go-web-image-orchestrator-kong.pnum-3000",
    "traceid_byte_count": 16,
    "local_service_name": "kong-go-prod",
    "tags_header": "Zipkin-Tags",
    "http_endpoint": "http://jaeger-prod-collector.observability.svc:9411/api/v2/spans",
    "include_credential": true,
    "tracing_header_name": "auth-token",
    "sample_ratio": 1,
    "default_header_type": "jaeger",
    "header_type": "preserve"
  }
}'
```
#### Kong Declarative
```
plugins:
- name: zipkin
  config:
    http_endpoint: http://jaeger-prod-collector.observability.svc:9411/api/v2/spans
    sample_ratio: 1
    include_credential: true
    traceid_byte_count: 16
    header_type: preserve
    default_header_type: jaeger
    tags_header: Zipkin-Tags
    tracing_header_name: auth-idsession
    local_service_name: kong-go-prod
```

```
plugins:
- name: zipkin
  config:
    service: id-dervice
    http_endpoint: http://jaeger-prod-collector.observability.svc:9411/api/v2/spans
    sample_ratio: 1
    include_credential: true
    traceid_byte_count: 16
    header_type: preserve
    default_header_type: jaeger
    tags_header: Zipkin-Tags
    tracing_header_name: auth-token
    local_service_name: kong-go-prod
```

## Comandos para desarrollar o actualizar plugins para Kong Gateway con Lua
```
luarocks make --lua-version=5.1
luarocks pack dtracing 1.0.0-2 --lua-version=5.1
luarocks upload dtracing-1.0.0-2.rockspec --api-key=GjIrHYWZ3kWwWaE1epXKv9N9KQSXaA17x5U7mLq5 --lua-version=5.1


docker build -t us-west2-docker.pkg.dev/servipag-preprod/tools/kong2.4.0-dtracing:v1.0.0 --no-cache --progress=plain .
docker push us-west2-docker.pkg.dev/servipag-preprod/tools/kong2.4.0-dtracing:v1.0.1

```
***




# Getting Started For Zipkin Plugin

## Get a running Zipkin instance

e.g. using docker:

```
sudo docker run -d -p 9411:9411 openzipkin/zipkin
```


## Enable the Plugin

```
curl --url http://localhost:8001/plugins/ -d name=zipkin -d config.http_endpoint=http://127.0.0.1:9411/api/v2/spans
```

See many more details of using this plugin at https://docs.konghq.com/plugins/zipkin/


# Implementation

The Zipkin plugin is derived from an OpenTracing base.

A tracer is created with the "http_headers" formatter set to use the headers described in [b3-propagation](https://github.com/openzipkin/b3-propagation)

## Spans

  - *Request span*: 1 per request. Encompasses the whole request in kong (kind: `SERVER`).
    The proxy span and balancer spans are children of this span.
    Contains logs/annotations for the `kong.rewrite` phase start and end
  - *Proxy span*: 1 per request. Encompassing most of Kong's internal processing of a request (kind: `CLIENT`)
    Contains logs/annotations for the rest start/finish of the of the Kong plugin phases:
    - `krs` - `kong.rewrite.start`
    - `krf` - `kong.rewrite.finish`
    - `kas` - `kong.access.start`
    - `kaf` - `kong.access.finish`
    - `kbs` - `kong.body_filter.start`
    - `kbf` - `kong.body_filter.finish`
    - `khs` - `kong.header_filter.start`
    - `khf` - `kong.header_filter.finish`
    - `kps` - `kong.preread.start`
    - `kpf` - `kong.preread.finish`
    This kind of information is useful for finding performance problems in plugins.
  - *Balancer span(s)*: 0 or more per request, each encompassing one balancer attempt (kind: `CLIENT`)
    Contains tags specific to the load balancing:
    - `kong.balancer.try`: a number indicating the attempt order
    - `peer.ipv4`/`peer.ipv6` + `peer.port` for the balanced port
    - `error`: true/false depending on whether the balancing could be done or not
    - `http.status_code`: the http status code received, in case of error
    - `kong.balancer.state`: an nginx-specific description of the error: `next`/`failed` for HTTP failures, `0` for stream failures.
      Equivalent to `state_name` in [OpenResty's Balancer's `get_last_failure` function](https://github.com/openresty/lua-resty-core/blob/master/lib/ngx/balancer.md#get_last_failure).

## Tags

### Standard tags

"Standard" tags are documented [here](https://github.com/opentracing/specification/blob/master/semantic_conventions.md)
Of those, this plugin currently uses:

  - `span.kind` (sent to Zipkin as "kind")
  - `http.method`
  - `http.status_code`
  - `http.path`
  - `error`
  - `peer.ipv4`
  - `peer.ipv6`
  - `peer.port`
  - `peer.hostname`
  - `peer.service`


### Non-Standard tags

In addition to the above standardised tags, this plugin also adds:

  - `component` (sent to Zipkin as "lc", for "local component")
  - `kong.api` (deprecated)
  - `kong.consumer`
  - `kong.credential`
  - `kong.node.id`
  - `kong.route`
  - `kong.service`
  - `kong.balancer.try`
  - `kong.balancer.state`

## Logs / Annotations

Logs (annotations in Zipkin) are used to encode the begin and end of every kong phase.

  - `kong.rewrite`, `start` / `finish`, `<timestamp>`
  - `kong.access`, `start` / `finish`, `<timestamp>`
  - `kong.preread`, `start` / `finish`, `<timestamp>`
  - `kong.header_filter`, `start` / `finish`, `<timestamp>`
  - `kong.body_filter`, `start` / `finish`, `<timestamp>`

They are transmitted to Zipkin as annotations where the `value` is the concatenation of the log name and the value.

For example, the `kong.rewrite`, `start` log would be transmitted as:

  - `{ "value" = "kong.rewrite.start", timestamp = <timestamp> }`


[badge-travis-url]: https://travis-ci.com/Kong/kong-plugin-zipkin/branches
[badge-travis-image]: https://travis-ci.com/Kong/kong-plugin-zipkin.svg?branch=master


## Comandos
```
luarocks make --lua-version=5.1
luarocks pack dtracing 1.0.0-2 --lua-version=5.1
luarocks upload dtracing-1.0.0-2.rockspec --api-key=GjIrHYWZ3kWwWaE1epXKv9N9KQSXaA17x5U7mLq5 --lua-version=5.1

docker build -t us-west2-docker.pkg.dev/servipag-preprod/tools/kong2.4.0-dtracing:v1.0.0 --no-cache --progress=plain .

docker push us-west2-docker.pkg.dev/servipag-preprod/tools/kong2.4.0-dtracing:v1.0.1

```
